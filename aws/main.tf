resource "google_compute_instance" "vm_instance" {
  #A unique name for the resource
  name         = var.name
  machine_type = var.machine_type
  boot_disk {
    initialize_params {
      #Image name
      image = var.image
    }
  }
  #Networks to attach to the instance
  network_interface {
    network = var.network

    access_config {
      // Ephemeral public IP
    }
  }
}
