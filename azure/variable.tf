variable "image" {
  type = string
  default = "debian-cloud/debian-9"
  description = "the name of the image"
}

variable "name" {
  type = string
  default = "test-1"
  description = "the name of the vm"
}

variable "network" {
  type = string
  default = "default"
  description = "the name of the network"
}

variable "machine_type" {
  type = string
  default = "e2-micro"
  description = "the name of the machine_type"
}
