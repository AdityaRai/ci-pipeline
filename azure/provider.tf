provider "google" {
 credentials = "${file("cred.json")}"
 project     = "aditya-gcp-project-361705"
 region      = "us-central1"
 zone        = "us-central1-a"
}
