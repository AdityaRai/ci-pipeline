instance_name = "test-tf"

machine_type = "n1-standard-1"

machine_zone = "us-east1-b"

boot_disk_image = "debian-11-bullseye-v20220719"

boot_disk_type = "pd-standard"
